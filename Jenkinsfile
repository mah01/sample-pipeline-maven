// see https://medium.com/@MichaKutz/create-a-declarative-pipeline-jenkinsfile-for-maven-releasing-1d43c896880c

pipeline {
    agent any

    options {
        buildDiscarder(
            logRotator(
                //artifactNumToKeepStr: '3',
                numToKeepStr: '5'
            )
        )
        disableConcurrentBuilds()
    }

    parameters {
        booleanParam(
            name: "RELEASE",
            description: "Build a release from current commit",
            defaultValue: false)
        string(
            name: 'RELEASE_VERSION',
            description: 'Release version (e.g. 3.0.0)',
            defaultValue: '#')
        string(
            name: 'NEXT_VERSION',
            description: 'Next version (e.g. 4.0.0). Do not add -SNAPSHOT, it will be added automatically',
            defaultValue: '#')
        booleanParam(
            name: "DEPLOY",
            description: "Deploy the artifact",
            defaultValue: false)
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        
        stage('Build') {
            steps {
                sh "mvn clean install"
            }
        }

        stage("Release") {
            when {
                expression { params.RELEASE }
            }
            steps {
                // cleanup any release state
                sh "mvn -B release:clean"

                sh "mvn -B release:prepare -DdevelopmentVersion=${params.NEXT_VERSION}-SNAPSHOT -DreleaseVersion=${params.RELEASE_VERSION} -Dtag=${params.RELEASE_VERSION}"

                // do not run "deploy" simply install the artifacts locally and skip tests since they ran just a second ago
                sh "mvn -B release:perform -Dgoals=install -DskipTests"
            }
        }

        stage("Deploy") {
            when {
                expression { params.DEPLOY }
            }

            steps {
                script {
                    // env.AVAILABLE_VERSIONS = getAvailableVersions()
                    env.AVAILABLE_VERSIONS = sh (
                            script: "ls -l ~/.m2/repository/testhest/testhest/ | awk '{ print \$9 }' | grep -v -e '^\$' | grep -v 'SNAPSHOT' | grep -v '.xml'",
                            returnStdout: true
                    )

                    DEPLOYMENT_PARAMETERS = input message: 'User input required',
                                            ok: 'Deploy',
                                            parameters: [
                                                    choice(name: 'VERSION_TO_DEPLOY', choices: env.AVAILABLE_VERSIONS, description: 'Version to deploy'),
                                                    choice(name: 'ENVIRONMENT', choices: "dev\ntest\nprod", description: 'Environment')
                                            ]
                }

                sh "./sample.sh ${DEPLOYMENT_PARAMETERS['VERSION_TO_DEPLOY']} ${DEPLOYMENT_PARAMETERS['ENVIRONMENT']}"
            }
        }
    }
}

def getAvailableVersions() {
    return "a\nb\nc\nd"
}
